package common;

public class Config {

    // Server bind
    //public static final String HOST = "*";
    public static final int PORT = 9908;

    // InterLoginServer
    public static final String ILS_HOST = "127.0.0.1";
    public static final int ILS_PORT = 9904;

    // Database
    public static final String DB_HOST = "localhost";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "";
    public static final int DB_PORT = 3306;
    public static final String DB_NAME = "apocalytica_gameserver";

    // Server properties
    public static final int SERVER_ID = 1;

    // Miscs
    private static boolean DEBUG = true;

    public Config() {

    }

}
