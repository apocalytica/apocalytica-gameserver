package common;

import communication.SQLFactory;
import objects.Character;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Loading {

    private static HashMap<Long, Character> characters = new HashMap<Long, Character>();

    public Loading() {

    }

    public static void loadCharacters(int accountID) {
        ResultSet result = SQLFactory.query("SELECT * FROM characters WHERE accountID = " + accountID);
        int i = 0;

        try {
            while (result.next()) {
                i++;
                characters.put(result.getLong("ID"),
                        new Character(
                                result.getLong("ID"),
                                result.getInt("accountID"),
                                result.getString("Name"),
                                result.getInt("Level"),
                                result.getInt("Life"),
                                result.getInt("MP"),
                                result.getLong("XP"),
                                result.getShort("Rights")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<Long, Character> getCharacters() {
        return characters;
    }

}
