package communication;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import common.Config;
import common.Utils;
import communication.parsing.InterDataAnalyzer;

import java.io.IOException;

public class InterServerFactory {

    private static Client client;
    private static boolean isRegistered;

    public InterServerFactory() {
        try {
            client = new Client();

            client.addListener(new Listener() {
                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof String) {
                        String packet = (String) object;
                        //System.out.println("Recv << " + packet);
                        InterDataAnalyzer.parse(packet);
                    }
                }

                @Override
                public void connected(Connection connection) {
                    System.out.println("Ok !");
                    System.out.print("Register the GameServer to the LoginServer : ");
                    sendPacket("GR" + Config.SERVER_ID + ";" + Config.PORT);
                }

                @Override
                public void disconnected(Connection connection) {
                    System.out.println("Connection from the LoginServer is lost !");
                }
            });

            client.start();
            client.connect(5000, Config.ILS_HOST, Config.ILS_PORT);
        } catch (IOException e) {
            System.out.println(" Failed !");
            e.printStackTrace();
            Utils.onErrorExit();
        }
    }

    public static Client getClient() {
        return client;
    }

    public static boolean getIsRegistered() {
        return isRegistered;
    }

    public static void setIsRegistered(boolean registered) {
        isRegistered = registered;
    }

    public void sendPacket(String packet) {
        client.sendTCP(packet);
    }

}
