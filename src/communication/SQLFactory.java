package communication;

import common.Config;
import common.Utils;

import java.sql.*;

public class SQLFactory {

    private static SQLFactory _self;
    private static Connection con;
    private static Statement stat;
    private static ResultSet result;

    public SQLFactory() {
        if (_self != null) {
            return;
        }
        _self = this;

        // Load driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Ok !");
        } catch (ClassNotFoundException e) {
            System.out.println("Failed !");
            e.printStackTrace();
            Utils.onErrorExit();
        }

        // Connect to database
        try {
            System.out.print("Connect to database : ");
            con = DriverManager.getConnection("jdbc:mysql://" + Config.DB_HOST + "/" + Config.DB_NAME,
                    Config.DB_USER, Config.DB_PASS);
            System.out.println("Ok !");
        } catch (SQLException e) {
            System.out.println("Unable to connect to database !");
            Utils.onErrorExit();
        }
    }

    public static ResultSet query(String query) {
        try {
            if (stat == null) {
                stat = con.createStatement();
            }
            if (result != null) {
                if (!result.isClosed()) {
                    result.close();
                    query(query);
                }
            }

            result = stat.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

}
