package communication;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import common.Config;
import communication.parsing.DataAnalyzer;
import objects.Client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class ServerFactory {

    private static Server server;
    private static HashMap<Integer, Client> clientList = new HashMap<Integer, Client>();
    private static HashMap<Integer, String> queueList = new HashMap<Integer, String>();

    public ServerFactory() {

        System.out.println("Blood Of Pain GameServer");
        System.out.print("Starting SQLFactory : ");
        new SQLFactory();

        System.out.print("Connection to LoginServer : ");
        new InterServerFactory();

        try {

            server = new Server();

            server.addListener(new Listener() {
                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof String) {
                        String packet = (String) object;
                        System.out.println("Recv << " + packet);
                        DataAnalyzer.parse(packet, clientList.get(connection.getID()));
                    }
                }

                @Override
                public void connected(Connection connection) {
                    System.out.println("Connection of " + connection.getRemoteAddressTCP());
                    Client client = new Client(connection);
                    clientList.put(connection.getID(), client);
                    client.sendPacket("Ti");
                }

                @Override
                public void disconnected(Connection connection) {
                    System.out.println("ClientID " + connection.getID() + " disconected");
                    removeClient(clientList.get(connection.getID()));
                }
            });

            while (!InterServerFactory.getIsRegistered()) // Block until the server is registered on the LonginServer
            {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            server.start();
            server.bind(Config.PORT);

            System.out.println("Server started on port " + Config.PORT + " !");
            System.out.println("Attempt client ...");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Server getServer() {
        return server;
    }

    public static void addClient(int conID, Client client) {
        clientList.put(conID, client);
    }

    public static void removeClient(int conID) {
        clientList.remove(conID);
    }

    public static void removeClient(Client client) {
        Iterator<Integer> kIter = clientList.keySet().iterator();
        Iterator<Client> vIter = clientList.values().iterator();

        while (kIter.hasNext() && vIter.hasNext()) {
            Integer key = kIter.next();
            Client value = vIter.next();

            if (value == client) {
                clientList.remove(key);
                System.out.println("Remove client " + client.getID());
            }
        }
    }

    public static Client getClient(int conID) {
        return clientList.get(conID);
    }

    public static void addToQueue(int accountID, String ticket) {
        queueList.put(accountID, ticket);
    }

    public static void removeFromQueue(int accountID) {
        queueList.remove(accountID);
    }

    public static boolean validQueuedClient(int accountID, String ticket) {
        String qTicket = queueList.get(accountID);
        if ((qTicket != null) && (qTicket.equals(ticket))) {
            return true;
        } else {
            return false;
        }
    }

}
