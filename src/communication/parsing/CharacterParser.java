package communication.parsing;

import common.Loading;
import objects.Character;
import objects.Client;

public class CharacterParser {

    public CharacterParser() {

    }

    public static void parse(String datas, Client client) {
        switch (datas.charAt(1)) {
            case 'R': // Random character name
                client.sendPacket("CR" + getCharacterNameGenerated());

                break;

            case 'S': // Select
                // Game is starting ...
                int internalCharID = Integer.parseInt(datas.substring(2));

                Character character = Loading.getCharacters().get(client.getCharacterList().get(internalCharID).getID());

                if (character != null) {
                    client.sendPacket("ES");
                } else {
                    client.disconnect();
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

    public static String getCharacterNameGenerated() {
        String Voyelles = "aeiouy";
        String Consonnes = "bcdfghjklmnpqrstvwxz";
        String nameGenerated = "";
        int lower = 0;
        int higher = 2;
        int random = (int) (Math.random() * (higher - lower)) + lower;
        int i = (int) Math.floor(Math.random() * 20);
        nameGenerated += Consonnes.toUpperCase().charAt(i);
        i = (int) Math.floor(Math.random() * 6);
        nameGenerated += Voyelles.charAt(i);
        i = (int) Math.floor(Math.random() * 6);
        nameGenerated += Voyelles.charAt(i);
        i = (int) Math.floor(Math.random() * 20);
        nameGenerated += Consonnes.charAt(i);
        i = (int) Math.floor(Math.random() * 20);
        nameGenerated += Consonnes.charAt(i);
        i = (int) Math.floor(Math.random() * 6);
        nameGenerated += Voyelles.charAt(i);
        if (random >= 1) {
            i = (int) Math.floor(Math.random() * 20);
            nameGenerated += Consonnes.charAt(i);
            i = (int) Math.floor(Math.random() * 6);
            nameGenerated += Voyelles.charAt(i);
        }

        return nameGenerated;
    }

}