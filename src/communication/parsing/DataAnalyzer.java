package communication.parsing;

import common.Loading;
import communication.ServerFactory;
import objects.Character;
import objects.Client;

import java.util.Iterator;

public class DataAnalyzer {

    public DataAnalyzer() {

    }

    public static void parse(String datas, Client client) {
        switch (datas.charAt(0)) {
            case 'C': // Character
                CharacterParser.parse(datas, client);

                break;

            case 'T': // Ticket
                switch (datas.charAt(1)) // Identification
                {
                    case 'i':
                        String[] tDatas = datas.substring(2).split(";");
                        int accountID = Integer.parseInt(tDatas[0]);

                        if (ServerFactory.validQueuedClient(accountID, tDatas[1])) {
                            ServerFactory.removeFromQueue(client.getID());
                            client.getAccount().setGUID(accountID);
                            Loading.loadCharacters(accountID);
                            client.sendPacket("CL" + getCharacterList(client));
                        } else {
                            // Send packet to say error
                            client.disconnect();
                        }

                        break;
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

    private static String getCharacterList(Client client) {
        StringBuilder sBuild = new StringBuilder();
        Iterator<Character> vIter = Loading.getCharacters().values().iterator();
        int i = 0;

        while (vIter.hasNext()) {
            Character value = vIter.next();
            if (value.getAccountID() == client.getAccount().getGUID()) {
                i++;
                client.addCharacterList(i, value);
                sBuild.append(i + ";");
                sBuild.append(value.getName() + ";");
                sBuild.append(value.getLevel() + ((vIter.hasNext()) ? "," : ""));
            }
        }

        return sBuild.toString();
    }

}
