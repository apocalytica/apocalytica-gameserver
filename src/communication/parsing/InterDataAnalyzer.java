package communication.parsing;

import communication.InterServerFactory;
import communication.ServerFactory;

public class InterDataAnalyzer {

    public InterDataAnalyzer() {

    }

    public static void parse(String datas) {
        switch (datas.charAt(0)) {
            case 'G': // GameServer
                switch (datas.charAt(1)) {
                    case 'R': // Register
                        if (datas.substring(2).equals("1")) {
                            System.out.println("Ok !");
                            InterServerFactory.setIsRegistered(true);
                        } else {
                            System.out.println("Failed !");
                        }

                        break;
                }

                break;

            case 'T': // Transfert
                switch (datas.charAt(1)) {
                    case 'r': // Ticket
                        String[] tDatas = datas.substring(2).split(";");

                        ServerFactory.addToQueue(Integer.parseInt(tDatas[0]), tDatas[1]);

                        break;
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
