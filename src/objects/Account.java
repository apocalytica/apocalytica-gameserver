package objects;


public class Account {

    private int GUID;
    private String name;
    private String password;
    private boolean banned;

    public Account() {

    }

    public Account(int pGUID, String pName, String pPassword, boolean pBanned) {
        this.GUID = pGUID;
        this.name = pName;
        this.password = pPassword;
        this.banned = pBanned;
    }

    public int getGUID() {
        return GUID;
    }

    public void setGUID(int gUID) {
        GUID = gUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

}
