package objects;

public class Character {

    private long ID;
    private int accountID;
    private String name;
    private int level;
    private int life;
    private int mana;
    private long xp;
    private short rights;
    /*private Inventory bag;*/ // TO DO Inventory

    public Character(long pID, int pAccountID, String pName, int pLevel, int pLife, int pMana, long pXP, short pRights) {
        this.ID = pID;
        this.accountID = pAccountID;
        this.name = pName;
        this.level = pLevel;
        this.life = pLife;
        this.mana = pMana;
        this.xp = pXP;
        this.rights = pRights;
    }

    public long getID() {
        return ID;
    }

    public void setID(long iD) {
        ID = iD;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public long getXp() {
        return xp;
    }

    public void setXp(long xp) {
        this.xp = xp;
    }

    public short getRights() {
        return rights;
    }

    public void setRights(short rights) {
        this.rights = rights;
    }

}
