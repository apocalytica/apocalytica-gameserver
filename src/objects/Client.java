package objects;

import com.esotericsoftware.kryonet.Connection;

import java.util.HashMap;

public class Client {

    private int ID;
    private Connection connection;
    private Account account;
    private String ticket;
    private HashMap<Integer, Character> characterList = new HashMap<Integer, Character>();

    public Client(Connection con) {
        this.ID = con.getID();
        this.connection = con;
        this.account = new Account();
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public void sendPacket(String packet) {
        System.out.println("Send (TCP) >> " + packet);
        this.connection.sendTCP(packet);
    }

    public void sendFasterPacket(String packet) {
        System.out.println("Send (UDP) >> " + packet);
        this.connection.sendUDP(packet);
    }

    public void disconnect() {
        this.connection.close();
    }

    public HashMap<Integer, Character> getCharacterList() {
        return characterList;
    }

    public void addCharacterList(int internalID, Character character) {
        this.characterList.put(internalID, character);
    }

}
