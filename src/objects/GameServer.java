package objects;

public class GameServer {

    private int ID;
    private String name;
    private String IP;
    private int TCPPort;
    private int UDPPort;
    private boolean isOnline;

    public GameServer(int pID, String pName, String pIP, int pTCPPort, int pUDPPort) {
        this.ID = pID;
        this.name = pName;
        this.IP = pIP;
        this.TCPPort = pTCPPort;
        this.UDPPort = pUDPPort;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String iP) {
        IP = iP;
    }

    public int getTCPPort() {
        return TCPPort;
    }

    public void setTCPPort(int tCPPort) {
        TCPPort = tCPPort;
    }

    public int getUDPPort() {
        return UDPPort;
    }

    public void setUDPPort(int uDPPort) {
        UDPPort = uDPPort;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

}
